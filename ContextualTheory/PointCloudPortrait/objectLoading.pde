final int HAMMER     = 0;
final int GLASSES    = 1;
final int PHONE      = 2;
final int PROSTHETIC = 3;
final int OOBE       = 4;
final int BCI        = 5;

final int techChoice = BCI;

Table fullTable, techTable;
float shapeScale;
int resolutionSkip  = 0;
float techPointDiam = 4;
float rotX          = 0;
float rotY          = 0;
float rotZ          = 0;
float posX          = 0;
float posY          = 0;
float posZ          = 0;

void loadSample(){
  techTable = new Table();

  switch(techChoice){
    case(HAMMER):
      fullTable = loadTable("hammer.csv", "csv");
      resolutionSkip = 12;
      shapeScale     = 0.18;
      rotZ           = PI;
      break;

    case(GLASSES):
      fullTable = loadTable("glasses.csv", "csv");
      resolutionSkip = 4;
      shapeScale     = 4.5;
      rotX           = PI * 1.05;
      break;

    case(PHONE):
      fullTable = loadTable("phone.csv", "csv");
      resolutionSkip = 10;
      shapeScale     = 25;
      rotX           = PI * 0.3;
      break;

    case(PROSTHETIC):
      fullTable = loadTable("prosthetic.csv", "csv");
      resolutionSkip = 3;
      shapeScale     = 180;
      rotX           = PI * 1.1;
      rotY           = PI;
      break;

    case(BCI):
      fullTable = loadTable("robotArmReduced4.csv", "csv");
      resolutionSkip = 1;
      shapeScale     = 50;
      rotX           = PI;
      rotY           = PI * 1.5;
      break;

    case(OOBE):
      fullTable = loadTable("oobeReduced22.csv", "csv");
      resolutionSkip = 5;
      shapeScale     = 500;
      rotX           = PI;
      break;
  }

  posX = kinectXMin + ((kinectXMax - kinectXMin) / 2);
  posY = kinectYMin + ((kinectYMax - kinectYMin) / 2);
  posZ = depthMin   + ((depthMax - depthMin) / 2);
}

void drawTechSample(){
  stroke(0, 0, 150);
  strokeWeight(techPointDiam / shapeScale);

  pushMatrix();
  translate(posX, posY, posZ);

  rotateX(rotX);
  rotateY(rotY);
  rotateZ(rotZ);

  scale(shapeScale);
  beginShape(POINTS);

  for (int r = 0; r < fullTable.getRowCount() - 1; r++){
    if (r % resolutionSkip == 0){
      vertex(fullTable.getFloat(r, 0),
             fullTable.getFloat(r, 1),
             fullTable.getFloat(r, 2)
             );
    }
  }
  endShape();

  popMatrix();
}
