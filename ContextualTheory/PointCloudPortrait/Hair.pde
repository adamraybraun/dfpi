class Hair{
  float z = random(-sphereRadius, sphereRadius);
  float phi = random(TWO_PI);
  float largo = random(1.15, 1.2);
  float theta = asin(z / sphereRadius);

  void draw(){
    float off  = (noise(millis() * 0.0005, sin(phi)) - 0.5) * 0.3;
    float offb = (noise(millis() * 0.0007, sin(z) * 0.01) - 0.5) * 0.3;

    float x = sphereRadius * cos(theta) * cos(phi);
    float y = sphereRadius * cos(theta) * sin(phi);
    float z = sphereRadius * sin(theta);

    float xo = sphereRadius * cos(theta + off) * cos(phi + offb);
    float yo = sphereRadius * cos(theta + off) * sin(phi + offb);
    float zo = sphereRadius * sin(theta + off);

    float xb = xo * largo;
    float yb = yo * largo;
    float zb = zo * largo;

    beginShape(LINES);
    stroke(0);
    vertex(x, y, z);
    stroke(200, 150);
    vertex(xb, yb, zb);
    endShape();
  }
}
