#!/bin/sh
# This script takes a .csv and copies every Nth line out of it into a new file
# Used to reduce the size of bloated .csv made from .OBJ's
sed -n '0~50p' robotArm.csv > robotArmReduced4.csv
