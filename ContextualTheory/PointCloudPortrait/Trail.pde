class Trail{
  private ArrayList<PVector> history = new ArrayList<PVector>();
  private PVector offset;
  private int trailLength;
  private color col;
  private int sectionLength = 5;

  Trail(int offsetX, int offsetY, int offsetZ, int trailLen, color colour){
    offset = new PVector(offsetX, offsetY, offsetZ);
    history.add(offset);
    trailLength = trailLen;
    col = colour;
  }

  void update(){
    PVector lastPos = history.get(0);
    PVector newPos = new PVector(
                                 lastPos.x = lerpedLoc.x + offset.x,
                                 lastPos.y = lerpedLoc.y + offset.y,
                                 lastPos.z = lerpedLoc.z + offset.z
                                );

    history.add(0, newPos);

    while (history.size() > trailLength) history.remove(trailLength);
  }

  void updateParams(int offsetX, int offsetY, int offsetZ, int trailLen, color colour){
    offset.x = offsetX;
    offset.y = offsetY;
    offset.z = offsetZ;
    trailLength = trailLen;
  }

  void display(){
    noFill();

    if (history.size() < trailLength - 1){
      return;
    }

    int increment = floor((sectionLength / 2));
    for (int v = 0; v < trailLength - increment - sectionLength; v += increment){
      float fadeVal = map(v, 0, trailLength - sectionLength, 0, trailMax);

      strokeWeight(fadeVal);
      stroke(col, fadeVal * 2);

      PVector curveStart = history.get(v);
      PVector curveMid1  = history.get(v + 1);
      PVector curveMid2  = history.get(v + 2);
      PVector curveMid3  = history.get(v + 3);
      PVector curveEnd   = history.get(v + 4);

      beginShape();
      curveVertex(curveStart.x, curveStart.y, curveStart.z);
      curveVertex(curveMid1.x,  curveMid1.y,  curveMid1.z);
      curveVertex(curveMid2.x,  curveMid2.y,  curveMid2.z);
      curveVertex(curveMid3.x,  curveMid3.y,  curveMid3.z);
      curveVertex(curveEnd.x,   curveEnd.y,   curveEnd.z);
      endShape();
    }
  }
}
