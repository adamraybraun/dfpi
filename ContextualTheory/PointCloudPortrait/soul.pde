final int NUM_TRAILS = 100;
final int NUM_HAIRS  = 4000;
boolean drawingSoul  = true;

Trail[] trails = new Trail[NUM_TRAILS];
int trailMax = 5;

Hair[] fur = new Hair[NUM_HAIRS];
int sphereRadius = 15;
float sphereXRot;
float sphereYRot;

void setupSoul(){
  // stetup trails
  for (int t = 0; t < NUM_TRAILS; t++){
    trails[t] = new Trail(int(random(-sphereRadius / 2, sphereRadius / 2)),
                          int(random(-sphereRadius / 2, sphereRadius / 2)),
                          int(random(-sphereRadius / 2, sphereRadius / 2)),
                          60,
                          color(255)
                          );
  }

  // setup sphere + fur
  for (int h = 0; h < NUM_HAIRS; h++){
    fur[h] = new Hair();
  }
  noiseDetail(15);
}

void drawSoul(){
  if (drawingSoul){
    // render trails
    for (int t = 0; t < NUM_TRAILS; t++){
      trails[t].update();
      trails[t].display();
    }

    // render sphere
    fill(0);
    noStroke();

    pushMatrix();
    translate(lerpedLoc.x, lerpedLoc.y, lerpedLoc.z);
    sphere(sphereRadius);

    // render hairs
    for (int h = 0; h < NUM_HAIRS; h++){
      fur[h].draw();
    }
    popMatrix();
  }
}

void updateTrails(){
  for (int t = 0; t < NUM_TRAILS; t++){
    trails[t].updateParams(int(random(-sphereRadius / 2, sphereRadius / 2)),
                           int(random(-sphereRadius / 2, sphereRadius / 2)),
                           int(random(-sphereRadius / 2, sphereRadius / 2)),
                           60,
                           color(255)
                           );
  }
}
