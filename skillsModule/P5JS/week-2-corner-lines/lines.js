const sketchSize = 800;
const LINE_MAX = 150;

var ascending = true;
var numLines = 1;

function setup(){
  createCanvas(sketchSize, sketchSize);

  stroke(0);
  strokeWeight(3);
}

function draw(){
  background(180);

  let spacing = sketchSize / numLines;

  for (let l = 1; l < numLines; l++){
    line(spacing, spacing * l, spacing * l, sketchSize - spacing);
  }

  if (ascending){
    numLines++;
    if (numLines > LINE_MAX) ascending = false;
  } else {
    numLines--;
    if (numLines < 1) ascending = true;
  }
}
