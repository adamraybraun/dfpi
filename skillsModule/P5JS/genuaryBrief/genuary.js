/*
  Genuary coding noodling around 3D flow fields

  Original flow field code inspired by; https://editor.p5js.org/ada10086/sketches/r1gmVaE07

  //TODO look into glowy shaders https://editor.p5js.org/kimberlypatton1774/sketches/7nUfLy17T
*/
var BACKGROUND_R = 22;
var BACKGROUND_G = 17;
var BACKGROUND_B = 23;

var num           = 3500;
var trailLength   = 7;
var noiseScale    = 1000;
var noiseStrength = 100;
var particles     = [num];
var sphereDiam    = 0.8;
var cols          = []
var border        = 200;

var frameSize, halfFrameSize, bgOffset;
let backgroundCanvas;

p5.disableFriendlyErrors = true; // marginally improves performance

function setup() {
  createCanvas(1080, 1080, WEBGL);

  // calculate vars once in setup
  frameSize     = width / 4;
  halfFrameSize = width / 2;

  noFill();
  // noStroke();
  strokeWeight(sphereDiam);

  // cols.push(color('#23294E')); // dark blue
  // cols.push(color('#541388')); // purple
  // cols.push(color('#F1E9DA')); // beige
  cols.push(color('#D90368')); // magenta
  cols.push(color('#FFFFFF'));
  // cols.push(color('#FFD400')); // yellow
  cols.push(color('#FFFFFF'));
  cols.push(color('#FFFFFF'));
  cols.push(color('#FFFFFF'));
  cols.push(color('#FFFFFF'));
  cols.push(color('#FFFFFF'));
  cols.push(color('#FFFFFF'));
  cols.push(color('#FFFFFF'));

  // generate backround once to store in memory
  backgroundCanvas = createGraphics(width - border, height - border);
  bgOffset = -halfFrameSize + border / 2;
  setupBackground();
  background(255);

  resetField(); // rest method also used to start
}

function draw() {
  // background texture
  image(backgroundCanvas, bgOffset, bgOffset);

  // rotate sphere of particles
  let rotateSpeed = -millis() / 10000;
  rotateY(rotateSpeed);
  rotateZ(rotateSpeed);

  // do that thang
  for (let i = 0; i < particles.length; i++) {
    // particles[i].run();
  }
}

function keyPressed(){
  if (key === ' '){
    resetField();
  } else if (key === '1'){
    noiseStrength = 0;
  } else if (key === '2'){
    noiseStrength = 10;
  } else if (key === '3'){
    noiseStrength = 50;
  } else if (key === '4'){
    noiseStrength = 100;
  } else if (key === '5'){
    noiseStrength = 300;
  } else if (key === '6'){
    noiseStrength = 600;
  } else if (key === '7'){
    noiseStrength = 1000;
  } else if (key === '8'){
    noiseStrength = 10000;
  }
}

// Clears particles, Re-seeds noise and starts new pattern of particles
function resetField(){
  let seed = millis();
  noiseSeed(seed);
  console.log(seed); // incase ya stumble accross a particularly tasty seed

  particles.length = 0;

  for (let i = 0; i < num; i++) {
    var loc      = createVector(random(-frameSize, frameSize),
                                random(-frameSize, frameSize),
                                random(-frameSize, frameSize));

    var dir      = createVector(cos(0), sin(0), sin(0));
    var speed    = random(0.2, 5.5);

    particles[i] = new Particle(loc, dir, speed, int(random(0, cols.length)));
  }
}

class Particle{
  constructor(_loc, _dir, _speed, _colIndex){
    this.loc      = _loc;
    this.dir      = _dir;
    this.speed    = _speed;
    this.trail    = [];
    this.colIndex = _colIndex
  }

  run() {
    this.move();
    this.checkEdgesSphere();
    this.update();
  }

  move(){
    // Calculate 3D perlin noise direction
    let angle = noise(this.loc.x / noiseScale,
                      this.loc.y / noiseScale,
                      this.loc.z / noiseScale) * noiseStrength;

    this.dir.x = cos(angle);
    this.dir.y = sin(angle);
    this.dir.z = sin(angle) * cos(angle);

    // Move towards perlin angle
    var vel = this.dir.copy();
    vel.mult(this.speed);
    this.loc.add(vel);

    // add new position to trail history
    this.trail.push(createVector(this.loc.x, this.loc.y, this.loc.z));

    // trim trail to max length
    if (this.trail.length > trailLength) this.trail.splice(0, 1);
  }

  // If a particle leaves sphere bounds then give it a new random position inside sphere
  checkEdgesSphere(){
    var distance = Math.sqrt(this.loc.x * this.loc.x +
                             this.loc.y * this.loc.y +
                             this.loc.z * this.loc.z);
    if (distance > frameSize){
      this.trail.length = 0;
      this.loc.x = random(-frameSize, frameSize);
      this.loc.y = random(-frameSize, frameSize);
      this.loc.z = random(-frameSize, frameSize);
    }
  }

  update(){
    stroke(cols[this.colIndex]);
    beginShape(); // solid lines
    // beginShape(TRIANGLES); // DOTTED lines
    for (let t = 0; t < this.trail.length; t++){
      let trailPos = this.trail[t];
      vertex(trailPos.x, trailPos.y, trailPos.z);
    }
    endShape();
  }
}

// 'paper' noise texture ripped from https://www.infoworld.com/article/3332676/javascript-tutorial-create-a-textured-paper-background-with-p5js.html
function setupBackground(){
  backgroundCanvas.background(BACKGROUND_R, BACKGROUND_G, BACKGROUND_B);
  const NUM_LINES = 150000;

  for(let i = 0; i < NUM_LINES; i++) {
    let x1     = Math.random() * width;
    let y1     = Math.random() * height;
    let theta  = Math.random() * 2 * Math.PI;
    let segLen = Math.random() * 5 + 5;
    let x2     = Math.cos(theta) * segLen + x1;
    let y2     = Math.sin(theta) * segLen + y1;

    backgroundCanvas.stroke(
      BACKGROUND_R,
      BACKGROUND_G + Math.random() * 20,
      BACKGROUND_B + Math.random() * 50,
      Math.random() * 5 + 75
    )
    backgroundCanvas.line(x1, y1, x2, y2);
  }
}
