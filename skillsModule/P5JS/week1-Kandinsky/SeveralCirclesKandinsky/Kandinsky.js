/**
  I made a 'brush' whose size / opacity I could alter with key commands
  I used it to sample colours from the original reference painting then console log
  the location / colour of each circle in a format that I could paste straight into the code.

  I had a go at applying this processing tutorial https://sighack.com/post/generative-watercolor-in-processing
  for watercolour-esqe gradients but had a really hard time implementing it so removed it.

  I'd certainly like to come back and crack a generative watercolour approach in the future :-)

  + I've commented out where I was loading the reference painting as an overlap as I couldn't work
  out an easy way to share with you in this format
**/

const CANVAS_WIDTH  = 800;
const CANVAS_HEIGHT = 800;

// reference image
let severalCirclesReference;
let referenceCanvas;
let showReference = false;

// brush variables
let brushR, brushG, brushB;
let brushSize    = 80;
let showBrush    = false;
let brushOpacity = 255;
let opacityShift = 10;

// background variables
const BACKGROUND_R = 22;
const BACKGROUND_G = 17;
const BACKGROUND_B = 23;
let backgroundCanvas;

function preload(){
  // severalCirclesReference = loadImage('assets/severalCircles.jpg');
}

function setup(){
  createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT);
  referenceCanvas = createGraphics(CANVAS_WIDTH, CANVAS_HEIGHT);
  // referenceCanvas.image(severalCirclesReference, 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

  backgroundCanvas = createGraphics(CANVAS_WIDTH, CANVAS_HEIGHT);
  setupBrush();
  setupBackground();
}

function setupBrush(){
  brushColour = color(0, 0, 0);
}

// 'paper' texture process ripped from https://www.infoworld.com/article/3332676/javascript-tutorial-create-a-textured-paper-background-with-p5js.html
function setupBackground(){
  backgroundCanvas.background(BACKGROUND_R, BACKGROUND_G, BACKGROUND_B);
  const NUM_LINES = 150000;
  for(let i = 0; i < NUM_LINES; i++) {
    let x1 = Math.random() * CANVAS_WIDTH;
    let y1 = Math.random() * CANVAS_HEIGHT;
    let theta = Math.random() * 2 * Math.PI;
    let segmentLength = Math.random() * 5 + 2;
    let x2 = Math.cos(theta) * segmentLength + x1;
    let y2 = Math.sin(theta) * segmentLength + y1;
    backgroundCanvas.stroke(
      BACKGROUND_R,
      BACKGROUND_G + Math.random() * 20,
      BACKGROUND_B + Math.random() * 40,
      Math.random() * 5 + 75
      // 0
    )
    backgroundCanvas.line(x1, y1, x2, y2);
  }
}

function draw(){
  drawBackgroundTexture();
  drawSolidCircles();

  // if (showReference) image(referenceCanvas, 0, 0);
  if (showBrush) displayBrushCursor();
}

function drawBackgroundTexture(){
  image(backgroundCanvas, 0, 0);
}

function drawSolidCircles(){
  noStroke();

  // peripheral circles
  fill(22, 138, 195);
  ellipse(153, 652, 40);

  fill(247, 149, 233);
  ellipse(112, 618, 21);

  fill(27, 29, 41);
  ellipse(108, 450, 87);

  fill(202, 189, 218);
  ellipse(109, 450, 78);

  fill(6, 4, 5);
  ellipse(120, 433, 28);

  fill(243, 136, 209);
  ellipse(98, 160, 66);

  fill(248, 224, 52);
  ellipse(78, 79, 30);

  fill(253, 229, 139);
  ellipse(578, 140, 56);

  fill(253, 237, 169);
  ellipse(578, 140, 56);

  fill(251, 230, 124);
  ellipse(578, 140, 36);

  fill(189, 202, 166);
  ellipse(658, 182, 33);

  fill(252, 174, 14);
  ellipse(658, 182, 26);

  fill(245, 50, 74);
  ellipse(704, 117, 25);

  fill(166, 218, 204);
  ellipse(715, 409, 28);

  fill(3, 53, 205);
  ellipse(715, 409, 20);

  fill(238, 219, 35);
  ellipse(637, 656, 19);

  fill(221, 38, 69);
  ellipse(617, 701, 28);

  fill(52, 139, 193);
  ellipse(704, 729, 24);

  fill(244, 185, 22);
  ellipse(399, 607, 31);

  fill(203, 208, 227, 235);
  ellipse(663, 563, 143);

  fill(213, 178, 52, 235);
  ellipse(741, 567, 20);

  fill(9, 1, 2, 235);
  ellipse(686, 510, 10);

  fill(228, 35, 45, 235);
  ellipse(668, 507, 15);

  fill(242, 223, 193, 195);
  ellipse(624, 548, 49);

  fill(146, 222, 229, 245);
  ellipse(623, 547, 34);

  fill(207, 179, 130, 245);
  ellipse(579, 535, 42);

  fill(47, 104, 94, 215);
  ellipse(567, 251, 128);

  fill(70, 165, 185, 195);
  ellipse(568, 249, 41);

  fill(28, 23, 29, 245);
  ellipse(568, 249, 26);

  fill(181, 183, 224, 175);
  ellipse(341, 474, 71);

  fill(11, 5, 5, 245);
  ellipse(321, 513, 10);

  fill(124, 127, 48, 245);
  ellipse(101, 678, 17);

  fill(24, 22, 18, 245);
  ellipse(101, 678, 13);

  fill(217, 25, 48, 165);
  ellipse(190, 536, 61);

  fill(254, 235, 101, 165);
  ellipse(159, 527, 58);

  fill(153, 74, 42, 165);
  ellipse(219, 535, 58);

  fill(153, 74, 42, 165);
  ellipse(235, 496, 58);

  fill(226, 179, 27, 165);
  ellipse(201, 461, 58);

  // central circle
  fill(21, 50, 204, 255);
  ellipse(342, 301, 353);

  fill(15, 11, 10, 255);
  ellipse(306, 270, 239);

  fill(122, 96, 51, 225);
  ellipse(458, 406, 105);

  fill(177, 148, 38, 225);
  ellipse(402, 407, 69);

  fill(173, 224, 181, 225);
  ellipse(429, 338, 67);

  fill(17, 58, 198, 205);
  ellipse(376, 359, 30);

  fill(249, 99, 175, 205);
  ellipse(493, 431, 41);

  fill(16, 12, 18, 245);
  ellipse(476, 445, 13);

  fill(68, 177, 216, 155);
  ellipse(455, 498, 91);

  fill(217, 145, 229, 155);
  ellipse(422, 172, 91);

  fill(222, 178, 237, 235);
  ellipse(273, 455, 11);

  fill(31, 18, 31, 235);
  ellipse(244, 456, 14);

  fill(213, 79, 121, 245);
  ellipse(244, 456, 10);
}

function displayBrushCursor(){
  push();
  fill(brushR, brushG, brushB, brushOpacity);
  ellipse(mouseX, mouseY, brushSize);
  pop();
}

function logBrushMark(){
  let brushLog = "Brush mark captured: \n\n";
  brushLog += "fill(" + brushR + ", " + brushG + ", " + brushB + ", " + brushOpacity + ");\n"
  brushLog += "ellipse(" + mouseX + ", " + mouseY + ", " + brushSize + ");\n";
  console.log(brushLog);
}

function keyTyped(){
  if (key === 'r'){
    showReference = !showReference;
  } else if (key === 'a'){
    if (brushOpacity > opacityShift) brushOpacity -= opacityShift;
  } else if (key === 's'){
    if (brushOpacity < 255 - opacityShift) brushOpacity  += opacityShift;
  } else if (key === 'b'){
    showBrush = !showBrush;
  } else if (key === 'c'){
    brushR = red(referenceCanvas.get(mouseX, mouseY));
    brushG = green(referenceCanvas.get(mouseX, mouseY));
    brushB = blue(referenceCanvas.get(mouseX, mouseY));
  } else if (key === '['){
    brushSize--;
  } else if (key === ']'){
    brushSize++;
  } else if (key === ' '){
    logBrushMark();
  }
}
