/**
  Cosmic doughnut
**/

const NUM_PLANETS = 1500;

let planetPosition   = {}
let planetDistance   = {};
let planetRadius     = {};
let planetSpeed      = {};
let planetHue        = {};
let planetSaturation = {};
let planetBrightness = {};
let planetOpacity    = {};

function setup(){
  createCanvas(1000, 1000);
  colorMode(HSB);
  noStroke();
  configurePlanets();

  frameRate(30);
}

function configurePlanets(){
  for (var p = 0; p < NUM_PLANETS; p++){
    planetSaturation[p] = random(50, 10);
    planetBrightness[p] = random(40, 80);
    planetDistance[p]   = random(width * 0.1, width * 0.35);
    planetPosition[p]   = random(0, 100);
    planetOpacity[p]    = random(0.01, 0.5);
    planetRadius[p]     = random(2, 30);
    planetSpeed[p]      = random(-0.01, 0.01);
    planetHue[p]        = random(0, 360);
  }
}

function draw(){
  background(0, 0.2);

  for (var p = 0; p < NUM_PLANETS; p++){
    planetHue[p] = shiftHue(planetHue[p]);

    planetPosition[p] = drawPlanet(planetSaturation[p],
                                   planetBrightness[p],
                                   planetPosition[p],
                                   planetDistance[p],
                                   planetOpacity[p],
                                   planetRadius[p],
                                   planetSpeed[p],
                                   planetHue[p]
                                  );
  }
}

function shiftHue(hue){
  return (hue += 0.5) % 360;
}

function drawPlanet(saturation, brightness, position, distance, opacity, radius, speed, hue){
  push();
  translate(width / 2, height / 2);
  rotate(position);
  fill(hue, saturation, brightness, opacity);
  ellipse(distance, 0, radius);
  pop();

  return position += speed;
}
