/*
  Vera Molar
  https://dam.org/museum/artists_ui/artists/molnar-vera/interruptions/

  Obervations:
  1.  grid of lines
  2. Each lines seems seemingly radomly laid out / maybe a flow field?
  3. Patches of sparses areas with no lines
  4. All framed in a square outline
  5. all have a consistent stroke weight
  6. all have a consitent colour
  7. All the lines have a constant length
  8.
  9.
  10.

*/

const NUM_ROWS    = 60;
const NUM_COLUMNS = 60;
const BORDER      = 30;

const LINE_LENGTH = 12;
const ANGLE_NOISE_SCALE = 0.001;
const EXCLU_NOISE_SCALE = 10;

function setup(){
  createCanvas(800, 800);
  drawMolar();
  angleMode(DEGREES);
}

function draw(){
  // drawMolar()
}

function drawMolar(){
  background(230);
  stroke(10);
  strokeWeight(1);

  for (let c = 1; c < NUM_COLUMNS - 1; c++){
    for (let r = 1; r < NUM_ROWS - 1; r++){

      let excluded = false;

      // if ((noise(c, r) * 100) > 60) excluded = true;

      if (excluded) continue;

      let lineCenterX = (((width - (BORDER * 2)) / NUM_COLUMNS) * c) + BORDER;
      let lineCenterY = (((height - (BORDER * 2)) / NUM_ROWS) * r) + BORDER;

      // get angle from 2D perlin field
      let angle = (noise(c * ANGLE_NOISE_SCALE, r * ANGLE_NOISE_SCALE) * 360);

      // selectively randomise some of those angles
      if (random() > 0.25) angle += noise(c * r) * 360;

      let lineStartX = lineCenterX + sin(angle) * LINE_LENGTH;
      let lineStartY = lineCenterY + cos(angle) * LINE_LENGTH;

      let lineEndX   = lineCenterX + sin(angle + 180) * LINE_LENGTH;
      let lineEndY   = lineCenterY + cos(angle + 180) * LINE_LENGTH;

      line(lineStartX, lineStartY, lineEndX, lineEndY);
    }
  }
}
