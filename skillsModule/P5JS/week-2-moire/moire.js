const sketchSize = 1000;

let moireCircle;

let radius = 0;
let angle = 0;
let motion = 1;
const speed = 0.05;

function setup(){
  createCanvas(sketchSize, sketchSize);

  generateGraphics();
}

function draw(){
  background(200);
  image(moireCircle, 0, 0);
  drawSpiralCircle();
}

function generateGraphics(){
  moireCircle = createGraphics(sketchSize, sketchSize);

  let numRings = 65;
  let spacing = 15;
  moireCircle.strokeWeight(3);

  moireCircle.noFill();

  for (let c = 0; c < numRings; c++){
    moireCircle.ellipse(sketchSize / 2, sketchSize / 2, spacing * c);
  }
}

function drawSpiralCircle(){
  let circleX = radius * sin(angle);
  var circleY = radius * cos(angle);

  image(moireCircle, circleX, circleY);

  if (radius > 450 && motion === 1){
    motion = -1;
  } else if (radius <= 0){
    motion = 1;
  }

  radius += motion;
  angle += speed;
}
